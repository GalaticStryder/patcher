#!/sbin/sh

OUTFD=/proc/self/fd/$2;
ZIP="$3";
DIR=`dirname "$ZIP"`;

ui_print() {
  until [ ! "$1" ]; do
    echo -e "ui_print $1\nui_print" > $OUTFD;
    shift;
  done;
}
show_progress() { echo "progress $1 $2" > $OUTFD; }
set_perm_recursive() {
  dirs=$(echo $* | $bb awk '{ print substr($0, index($0,$5)) }');
  for i in $dirs; do
    $bb chown -R $1:$2 $i;
    find "$i" -type d -exec chmod $3 {} +;
    find "$i" -type f -exec chmod $4 {} +;
  done;
}
file_getprop() { grep "^$2" "$1" | cut -d= -f2; }
getprop() { test -e /sbin/getprop && /sbin/getprop $1 || file_getprop /default.prop $1; }
sysprop() { file_getprop /system/build.prop $1; }
cleanup() { rm -rf /tmp/patcher; }
abort() { ui_print "$*"; cleanup; umount /system; exit 1; }

show_progress 1.34 4;
ui_print " ";
ui_print "------- ~ ------- ~ -------";
ui_print "   __ _ ___ ____ __  __ _  ";
ui_print "  / _` / _ (_-< '_ \/ _` | ";
ui_print "  \__,_\___/__/ .__/\__,_| ";
ui_print "              |_|          ";
ui_print "                           ";
ui_print "------- ~ Patcher ~ -------";
ui_print " ";
cleanup;
mkdir -p /tmp/patcher/bin;
cd /tmp/patcher;
unzip -o "$ZIP";
if [ $? != 0 -o -z "$(ls /tmp/patcher/tools)" ]; then
  abort "Unzip failed. Aborting...";
fi;
chmod -R 755 /tmp/patcher/tools /tmp/patcher/bin;
bb=/tmp/patcher/tools/busybox;

ui_print "Mounting system partition...";
umount /system;
mount -o ro -t auto /system;

ui_print "Checking AOSPA version...";
paranoid="$(sysprop ro.pa.version)";
ui_print "Paranoid Version: $paranoid.";
testpa="$(file_getprop /tmp/patcher/patcher.sh pa.version)";
if [ "$paranoid" == "$testpa" ]; then
    ui_print "Passed!";
    matchpa=1;
fi;
if [ "$matchpa" != 1 ]; then
    abort "Sorry, this version is not supported. Aborting...";
fi;

if [ "$(file_getprop /tmp/patcher/patcher.sh do.devicecheck)" == 1 ]; then
  ui_print "Checking device...";
  for i in 1 2 3 4 5; do
    testname="$(file_getprop /tmp/patcher/patcher.sh device.name$i)";
    if [ "$(getprop ro.product.device)" == "$testname" -o "$(getprop ro.build.product)" == "$testname" ]; then
      ui_print "Your $testname is supported.";
      ui_print "Passed!";
      match=1;
    fi;
  done;
  ui_print " ";
  if [ "$match" != 1 ]; then
    abort "Sorry, your device is not supported. Aborting...";
  fi;
fi;

ui_print "Setting up busybox binary...";
for i in $($bb --list); do
  $bb ln -s $bb /tmp/patcher/bin/$i;
done;
if [ $? != 0 -o -z "$(ls /tmp/patcher/bin)" ]; then
  abort "Recovery busybox setup failed. Aborting...";
fi;
ui_print "Running patcher script...";
PATH="/tmp/patcher/bin:$PATH" $bb ash /tmp/patcher/patcher.sh $2;
if [ $? != "0" ]; then
  abort;
fi;

if [ "$(file_getprop /tmp/patcher/patcher.sh do.patch)" == 1 ]; then
    mount -o rw,remount -t auto /system;
    ui_print "Updating audio configuration...";
    cp -f /tmp/patcher/patch/audio_platform_info.xml /system/etc/audio_platform_info.xml;
    cp -f /tmp/patcher/patch/mixer_paths_tasha.xml /system/etc/mixer_paths_tasha.xml;
    chmod 0644 /system/etc/audio_platform_info.xml;
    chmod 0644 /system/etc/mixer_paths_tasha.xml;
    mount -o ro -t auto /system;
fi;

if [ "$(file_getprop /tmp/patcher/patcher.sh do.cleanup)" == 1 ]; then
  ui_print "Removing patcher files...";
  cleanup;
fi;

umount /system;
ui_print " ";
ui_print "Patched up!";
ui_print " ";
