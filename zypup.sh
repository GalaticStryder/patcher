set -e
HERE=`pwd`
source $HERE/version.sh
zip -x@zipexclude -r9 paranoid_patcher-$version.zip *
