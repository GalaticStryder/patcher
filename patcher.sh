# Patcher script for AOSPA 7.x
# by GalaticStryder.

OUTFD=/proc/self/fd/$1;
patch=/tmp/patcher/patch;
#block=/dev/block/bootdevice/by-name/boot;

properties() {
do.devicecheck=1
do.cleanup=1
do.patch=1
device.name1=zl1
device.name2=le_zl1
device.name3=
device.name4=
device.name5=
pa.version=7.2.0-RELEASE
}

ui_print() { echo -e "ui_print $1\nui_print" > $OUTFD; }

source /tmp/patcher/version.sh;
ui_print "Version: $version.";

#ui_print "Pushing boot image to partition...";
#dd if=/dev/zero of=$block;
#dd if=$patch/boot.img of=$block;
